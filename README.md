# OpenML dataset: energy-efficiency

https://www.openml.org/d/1472

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Angeliki Xifara, Athanasios Tsanas 

**Source**: UCI

**Please cite**:   

Source:
  
The dataset was created by Angeliki Xifara (angxifara @ gmail.com, Civil/Structural Engineer) and was processed by Athanasios Tsanas (tsanasthanasis @ gmail.com, Oxford Centre for Industrial and Applied Mathematics, University of Oxford, UK).

Data Set Information:
  
We perform energy analysis using 12 different building shapes simulated in Ecotect. The buildings differ with respect to the glazing area, the glazing area distribution, and the orientation, amongst other parameters. We simulate various settings as functions of the afore-mentioned characteristics to obtain 768 building shapes. The dataset comprises 768 samples and 8 features, aiming to predict two real valued responses. It can also be used as a multi-class classification problem if the response is rounded to the nearest integer.
 
Attribute Information:
 
The dataset contains eight attributes (or features, denoted by X1...X8) and two responses (or outcomes, denoted by y1 and y2). The aim is to use the eight features to predict each of the two responses. 

Specifically: 
X1  Relative Compactness 
X2  Surface Area 
X3  Wall Area 
X4  Roof Area 
X5  Overall Height 
X6  Orientation 
X7  Glazing Area 
X8  Glazing Area Distribution 
y1  Heating Load 
y2  Cooling Load

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1472) of an [OpenML dataset](https://www.openml.org/d/1472). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1472/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1472/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1472/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

